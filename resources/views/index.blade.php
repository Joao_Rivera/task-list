@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <task-lists-main :task-lists="taskLists" :selected-list="selectedList"></task-lists-main>
        </div>
    </div>
</div>
@endsection