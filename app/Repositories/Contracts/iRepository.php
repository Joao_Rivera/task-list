<?php
/**
 * Created by PhpStorm.
 * User: joao2
 * Date: 8/8/2018
 * Time: 2:53 PM
 */

namespace App\Repositories\Contracts;


interface iRepository
{
    public function with($relationships);

    public function where($column, $operator_or_value = null, $value = null);

    public function create(array $data);

    public function find($id);

    public function all();

    public function update(array $data, $id);

    public function delete($id);
}