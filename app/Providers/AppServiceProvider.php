<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        $this->app->bind(
            \App\Repositories\Contracts\iTaskListRepository::class,
            \App\Repositories\Eloquent\TaskListRepository::class
        );

        $this->app->bind(
            \App\Repositories\Contracts\iTaskRepository::class,
            \App\Repositories\Eloquent\TaskRepository::class
        );
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
