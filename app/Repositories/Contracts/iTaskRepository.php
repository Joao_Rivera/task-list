<?php
/**
 * Created by PhpStorm.
 * User: joao2
 * Date: 8/8/2018
 * Time: 3:49 PM
 */

namespace App\Repositories\Contracts;

interface iTaskRepository extends iRepository
{
    /*public function addInTaskList(array $task, $task_list_id);

    public function allByTaskList($task_list_id);*/

    public function belongsToUser($id, $user_id);

    public function belongsToSession($id, $session_key);

}