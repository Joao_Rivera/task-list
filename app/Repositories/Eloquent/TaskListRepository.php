<?php
/**
 * Created by PhpStorm.
 * User: joao2
 * Date: 8/8/2018
 * Time: 2:45 PM
 */

namespace App\Repositories\Eloquent;


use App\Models\Task;
use App\Models\TaskList;
use App\Repositories\Contracts\iTaskListRepository;

class TaskListRepository extends BaseRepository implements iTaskListRepository
{

    public function __construct()
    {
        $this->modelClass = TaskList::class;
    }

    public function createForUser(array $data, $user_id)
    {
        return $this->create(array_merge($data,['user_id'=>$user_id]));
    }

    public function createForSession(array $data, $session_key)
    {
        return $this->create(array_merge($data,['session_key'=>$session_key]));
    }

    public function allByUser($user_id)
    {
        return $this->query()->where('user_id',$user_id)->get();
    }

    public function allBySession($session_key)
    {
        return $this->query()->where('session_key',$session_key)->get();
    }

    public function firstBySession($session_key)
    {
        return $this->model = $this->query()->where('session_key',$session_key)->first();
    }


    public function findWithTasks($id)
    {
        $this->updateModel($id);
        return ($temp = $this->model)->load('tasks');
    }

    public function belongsToUser($id, $user_id)
    {
        $this->updateModel($id);
        return isset($this->model->user_id) && $this->model->user_id == $user_id;
    }

    public function belongsToSession($id, $session_key)
    {
        $this->updateModel($id);
        return isset($this->model->session_key) && $this->model->session_key == $session_key;
    }

    public function updateOwnershipToUser($session_key, $user_id)
    {
        return $this->query()->where('session_key',$session_key)->update(['session_key' => null, 'user_id' => $user_id]);

    }

    public function addTask(array $data, $id)
    {
        $this->updateModel($id);

        return $this->model->tasks()->create($data);
    }
}