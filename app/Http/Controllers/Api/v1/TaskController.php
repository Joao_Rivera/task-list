<?php

namespace App\Http\Controllers\Api\v1;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use App\Repositories\Contracts\iTaskRepository as Repository;
use App\Http\Resources\Task as TaskResource;

class TaskController extends Controller
{
    protected $repo;

    public function __construct(Repository $repository)
    {
        $this->repo = $repository;
    }

    public function update(Request $request, $id)
    {
        $data = $request->validate([
            'description' => 'max:255',
            'due_date' => 'nullable|date|date_format:Y-m-d',
            'priority' => 'digits_between:1,3',
            'status' => 'boolean',
        ]);

        $this->checkOwnership($id);

        if($this->repo->update($data, $id))
        {
            return (new TaskResource($this->repo->find($id)));
        }

        return response()->json(['success' => false, 'message' => 'Not Updated'],500);
    }

    public function destroy($id)
    {
        $this->checkOwnership($id);

        if($this->repo->delete($id))
        {
            return response()->json(['success' => true, 'message' => 'Deleted']);
        }

        return response()->json(['success' => false, 'message' => 'Not Deleted'],500);
    }

    public function setStatus(Request $request, $id)
    {
        $data = $request->validate([
            'status' => 'required|boolean',
        ]);

        $this->checkOwnership($id);

        if($this->repo->update($data, $id))
        {
            return (new TaskResource($this->repo->find($id)));
        }

        return response()->json(['success' => false, 'message' => 'Not Updated'],500);
    }

    protected function hasOwnership($id)
    {
        return Auth::check() && $this->repo->belongsToUser($id, Auth::id())
            || $this->repo->belongsToSession($id, Session::getId());
    }

    protected function checkOwnership($id)
    {
        if (!$this->hasOwnership($id))
        {
            throw new NotFoundHttpException('Not Found');
        }
    }
}
