<?php

use Faker\Generator as Faker;
use App\Models\TaskList;
use App\Models\Task;

$factory->define(TaskList::class, function (Faker $faker) {
    return [
        'name' => $faker->sentence(3),
        'user_id' => factory(App\User::class),
    ];
});

$factory->state(TaskList::class, 'guest', function (Faker $faker) {
    return [
        'user_id' => null,
        'session_key' => $faker->uuid(),
    ];
});

$factory->state(TaskList::class, 'filled', function (Faker $faker) {
    return [];
});


$factory->afterCreatingState(TaskList::class, 'filled', function (TaskList $taskList, Faker $faker) {
    $taskList->tasks()->saveMany(factory(Task::class,3)->make([
        'task_list_id' => null,
    ]));
});
