<?php

namespace App\Http\Controllers\Api\v1;

use App\Http\Controllers\Controller;
use App\Repositories\Contracts\iTaskListRepository as Repository;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use App\Http\Resources\Task as TaskResource;
use App\Http\Resources\TaskList as TaskListResource;
use App\Http\Resources\TaskLists as TaskListsCollection;

class TaskListController extends Controller
{

    protected $repo;

    public function __construct(Repository $repository)
    {
        $this->repo = $repository;
    }

    public function index()
    {
        $task_lists = Auth::check()
            ? $this->repo->allByUser(Auth::id())
            : $this->repo->allBySession(Session::getId());

        return new TaskListsCollection($task_lists);
    }

    public function show($id)
    {
        $this->checkOwnership($id);

        return new TaskListResource($this->repo->findWithTasks($id));
    }

    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required|max:255',
        ]);

        $ownership = $this->getOwnership();

        $data = [
            'name' => $request->input('name'),
            $ownership['key'] => $ownership['value']
        ];

        return (new TaskListResource($this->repo->create($data)));
    }

    public function update(Request $request, $id)
    {
        $request->validate([
            'name' => 'required|max:255',
        ]);

        $this->checkOwnership($id);

        $ownership = $this->getOwnership();

        $data = [
            'name' => $request->input('name'),
            $ownership['key'] => $ownership['value']
        ];

        if($this->repo->update($data, $id))
        {
            return (new TaskListResource($this->repo->find($id)));
        }

        return response()->json(['success' => false, 'message' => 'Not Updated'],500);
    }

    public function destroy($id)
    {
        $this->checkOwnership($id);

        if(Auth::guest())
        {
            \session()->regenerate();
        }

        if($this->repo->delete($id))
        {
            return response()->json(['success' => true, 'message' => 'Deleted']);
        }

        return response()->json(['success' => false, 'message' => 'Not Deleted'],500);
    }

    public function addTask(Request $request, $id){

        $data = $request->validate([
            'description' => 'required|max:255',
            'due_date' => 'nullable|date|date_format:Y-m-d',
            'priority' => 'digits_between:1,3',
            'status' => 'boolean',
        ]);

        $this->checkOwnership($id);

        return (new TaskResource($this->repo->addTask($data, $id)));
    }

    protected function getOwnership()
    {
        if(Auth::check())
        {
            return ['key'=> 'user_id', 'value' => Auth::id()];
        }
        return ['key'=> 'session_key', 'value' => Session::getId()];
    }

    protected function hasOwnership($id)
    {
        return Auth::check() && $this->repo->belongsToUser($id, Auth::id())
            || $this->repo->belongsToSession($id, Session::getId());
    }

    protected function checkOwnership($id)
    {
        if (!$this->hasOwnership($id))
        {
            throw new NotFoundHttpException('Not Found');
        }
    }
}
