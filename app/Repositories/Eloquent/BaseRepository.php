<?php
/**
 * Created by PhpStorm.
 * User: joao2
 * Date: 8/8/2018
 * Time: 3:47 PM
 */

namespace App\Repositories\Eloquent;


use App\Repositories\Contracts\iRepository;

class BaseRepository implements iRepository
{

    protected $modelClass;

    protected $model;

    protected $query;

    public function all()
    {
        return $this->query()->all();
    }

    public function find($id)
    {
        return $this->model = $this->query()->find($id);
    }

    public function create(array $data)
    {
        return $this->model = $this->query()->create($data);
    }

    public function update(array $data, $id)
    {
        $this->updateModel($id);
        return $this->model->update($data);
    }

    public function delete($id)
    {
        $this->updateModel($id);
        return $this->model->delete();
    }

    protected function query(){
        if(!isset($this->query)){
            $this->query = $this->modelClass::query();
        }
        return $this->query;
    }

    public function with($relationships)
    {
        $this->query = $this->query()->with($relationships);

        return $this;
    }

    public function where($column, $operator_or_value = null, $value = null)
    {
        $this->query =  $this->query()->where($column, $operator_or_value, $value);

        return $this;
    }

    protected function initModel(){
        if (!isset($this->model)){
            $this->model = new $this->modelClass;
        }
    }

    protected function unsetModel(){
        if (isset($this->model)){
            unset($this->model);
        }
    }

    protected function updateModel($id)
    {
        if (!isset($this->model) || $this->model->id != $id) {
            $this->find($id);
        }
    }

}