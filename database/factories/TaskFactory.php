<?php

use Faker\Generator as Faker;
use App\Models\Task;
use App\Models\TaskList;

$factory->define(Task::class, function (Faker $faker) {
    return [
        'description' => $faker->sentence(5),
        'status' => $faker->randomElement($array = array (0,1)),
        'due_date' => $faker->date(),
        'priority' => $faker->randomElement($array = array (1,2,3)),
        'task_list_id' => factory(TaskList::class),
    ];
});
