<?php
/**
 * Created by PhpStorm.
 * User: joao2
 * Date: 8/8/2018
 * Time: 2:45 PM
 */

namespace App\Repositories\Eloquent;


use App\Models\Task;
use App\Models\TaskList;
use App\Repositories\Contracts\iTaskRepository;

class TaskRepository extends BaseRepository implements iTaskRepository
{

    public function __construct()
    {
        $this->modelClass = Task::class;
    }

    /*public function allByTaskList($task_list_id)
    {
        $this->modelClass::where('task_list_id',$task_list_id)->get();
    }

    public function addInTaskList(array $data, $task_list_id)
    {
        $this->create(array_merge($data,['task_list_id'=>$task_list_id]));
    }*/

    public function belongsToUser($id, $user_id)
    {
        $this->updateModel($id);
        return isset($this->model->task_list->user_id) && $this->model->task_list->user_id == $user_id;
    }

    public function belongsToSession($id, $session_key)
    {
        $this->updateModel($id);
        return isset($this->model->task_list->session_key) && $this->model->task_list->session_key == $session_key;
    }
}