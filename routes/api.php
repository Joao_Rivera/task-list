<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::group(['prefix'=>'v1','namespace' => 'Api\v1'], function () {
    Route::group(['prefix'=>'task_lists', 'as'=>'task_lists.'], function(){
        Route::get('/', ['as'=>'index','uses' =>'TaskListController@index']);
        Route::get('/{id}', ['as'=>'show','uses' =>'TaskListController@show']);
        Route::post('/', ['as'=>'store','uses' =>'TaskListController@store']);
        Route::put('/{id}', ['as'=>'update','uses' =>'TaskListController@update']);
        Route::delete('/{id}', ['as'=>'destroy','uses' =>'TaskListController@destroy']);

        Route::post('/{id}/tasks', ['as'=>'add_task','uses' =>'TaskListController@addTask']);

    });

    Route::group(['prefix'=>'tasks', 'as'=>'tasks.'], function(){
        Route::put('/{id}', ['as'=>'update','uses' =>'TaskController@update']);
        Route::patch('/{id}', ['as'=>'update','uses' =>'TaskController@update']);
        Route::delete('/{id}', ['as'=>'destroy','uses' =>'TaskController@destroy']);
        Route::patch('/{id}/set_status', ['as'=>'set_status','uses' =>'TaskController@setStatus']);
    });
});