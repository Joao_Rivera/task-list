
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

window.Vue = require('vue');

window.BootstrapVue = require('bootstrap-vue');

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

Vue.component('example-component', require('./components/ExampleComponent.vue'));
Vue.component('task-lists-main', require('./components/TaskLists/TaskListMain'));

const app = new Vue({
    el: '#app',
    data : {
        apiUrl: '/api/v1/',
        taskLists: [],
        selectedList: {},
        apiConfig : {
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded',
                'Accept': 'application/json'
            }
        }
    },
    methods: {
        fetchTaskLists() {
            axios.get(this.apiUrl + 'task_lists').then(response =>{

                if(response.data.data.length === 0){
                    this.createTaskList({'name':'My first List'})
                }
                else {
                    Vue.set(this, 'taskLists', response.data.data);
                }
            });
        },
        showTaskList(list) {
            axios.get(this.apiUrl + 'task_lists/'+list.id).then(response =>{
                this.selectedList = response.data.data;
            });
        },
        createTaskList(list) {
            axios.post(this.apiUrl + 'task_lists/', list).then(response => {
                Vue.set(this.taskLists, this.taskLists.length, response.data.data)
            });
        },
        updateTaskList(list) {
            axios.put(this.apiUrl + 'task_lists/' + list.id, list).then(response => {
                Vue.set(this.taskLists, this.taskLists.length, response.data);
                this.fetchTaskLists();
            });

        },
        deleteTaskList(list) {
            axios.delete(this.apiUrl + 'task_lists/' + list.id,).then(response => {
                this.taskLists = this.taskLists.filter(t => t !== list );
            });
        },
        createTask(task) {
            var url = this.apiUrl + 'task_lists/' + this.selectedList.id + '/tasks';
            axios.post(url, task).then(response => {
                Vue.set(this.selectedList.tasks, this.selectedList.tasks.length, response.data.data)
            });
        },
        updateTask(task) {
            axios.patch(this.apiUrl + 'tasks/' + task.id, task, this.apiConfig).then(response => {
                //Vue.set(task, 'id', response.data.data.id)
                console.log(response.data);
            });
        },

        deleteTask(task) {
            axios.delete(this.apiUrl + 'tasks/' + task.id,).then(response => {
                this.selectedList.tasks = this.selectedList.tasks.filter(t => t !== task );
            });
        }

    },
    created() {
        this.$root.$on('createdTaskList', this.createTaskList);
        this.$root.$on('updatedTaskList', this.updateTaskList);
        this.$root.$on('deletedTaskList', this.deleteTaskList);
        this.$root.$on('selectedList', this.showTaskList);

        this.$root.$on('createdTask', this.createTask);
        this.$root.$on('updatedTask', this.updateTask);
        this.$root.$on('deletedTask', this.deleteTask);
        this.$root.$on('deletedTask', this.deleteTask);

        this.fetchTaskLists();

    }
});
