<?php
/**
 * Created by PhpStorm.
 * User: joao2
 * Date: 8/8/2018
 * Time: 3:49 PM
 */

namespace App\Repositories\Contracts;

interface iTaskListRepository extends iRepository
{
    public function createForUser(array $data, $user_id);

    public function createForSession(array $data, $session_key);

    public function allByUser($user_id);

    public function allBySession($session_key);

    public function firstBySession($session_key);

    public function findWithTasks($id);

    public function belongsToUser($id, $user_id);

    public function belongsToSession($id, $session_key);

    public function updateOwnershipToUser($session_key, $user_id);

    public function addTask(array $data, $id);

}